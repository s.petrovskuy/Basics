def char_freq(string):
    frequency_listing = {}
    for letter in string:
        if letter in frequency_listing:
            frequency_listing[letter] += 1
        else:
            frequency_listing[letter] = 1
    return frequency_listing
