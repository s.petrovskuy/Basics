import random


def game(a, b):
    secret_number = random.randint(a, b)
    while secret_number != int(input("Enter an integer number: ")):
        print("Try again!")
    print("Congratulation, you won!")
