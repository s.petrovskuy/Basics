def sum(list_of_numbers):
    total = 0
    for number in list_of_numbers:
        total += number
    return total


def multiply(list_of_numbers):
    total = 1
    for number in list_of_numbers:
        total *= number
    return total
