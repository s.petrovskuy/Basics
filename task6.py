import string


def ceaser_cipher(string_to_encrypt, key):
    list_of_ascii = string.ascii_lowercase
    encrypted = ''
    for word in string_to_encrypt.split(' '):
        for letter in word:
            encrypted += list_of_ascii[list_of_ascii.index(letter) - key]
        encrypted += ' '
    return encrypted

