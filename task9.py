def check_brackets(string):
    closing_bracket_count = 0
    for bracket in string:
        if bracket == '[':
            closing_bracket_count += 1
        else:
            closing_bracket_count -= 1
        if closing_bracket_count < 0:
            return "NOT OK"

    if closing_bracket_count != 0:
        return "NOT OK"
    return "OK"
