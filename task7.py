def diaginal_reverse(matrix):
    reversed_matrix = [[x for x in range(len(matrix))] for y in range(len(matrix))]
    for raw_index, raw in enumerate(matrix):
        for element_index, element in enumerate(raw):
            reversed_matrix[element_index][raw_index] = element
    return reversed_matrix
