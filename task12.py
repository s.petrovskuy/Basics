import random


def game(integer):
    x = random.randint(0, integer)
    y = random.randint(0, integer)
    matrix = []
    for i in range(integer):
        row = []
        for j in range(integer):
            row.append('*')
        matrix.append(row)
    while True:
        var1, var2 = input("Enter two numbers here: ").split()
        if int(var1) == x and int(var2) == y:
            print("Congratulation, you won!")
            break
        else:
            matrix[int(var1)][int(var2)] = 'x'
            for i in matrix:
                print(i)
            print("Try again!")
