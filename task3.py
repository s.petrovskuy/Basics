def reverse(string):
    reversed_string = ''
    size = len(string)
    while size > 0:
        reversed_string += string[size - 1]
        size -= 1
    return reversed_string


def reverse_another_solution(string):
    return string[::-1]
