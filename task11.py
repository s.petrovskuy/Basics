def dec_to_bin(integer):
    output = ''
    while integer != 0:
        output += str(integer % 2)
        integer = integer // 2
    print(output[::-1])
